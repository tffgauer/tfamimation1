//
//  ViewController.m
//  TFAnimation1
//
//  Created by Franklin Gauer on 9/7/14.
//  Copyright (c) 2014 TransFirst. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

UIImageView *dot;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Notification way of altering of orientation change. Didn't quite work. The screen orientation indicator is not
    // changed yet when this is fired.
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(deviceOrientationDidChangeNotification:)
     name:UIDeviceOrientationDidChangeNotification
     object:nil];
    
    [self drawImage];
}

-(void)drawImage
{
    int imageWidth = 200;
    int imageHeight = 200;
    
    CGRect orientationCorrectScreenBounds = [self currentScreenBoundsDependOnOrientation];
    int screenWidth = orientationCorrectScreenBounds.size.width;
    int screenHeight = orientationCorrectScreenBounds.size.height;
    
    dot =[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth / 2) - (imageWidth /2 ),(screenHeight / 2) - (imageHeight / 2),200,200)];
    dot.image=[UIImage imageNamed:@"TFLogo1.jpg"];
    
    [self.view addSubview:dot];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    NSLog(@"HELLO");
    [dot removeFromSuperview];
    [self drawImage];
}

// When this is fired, it does not have the correct status bar orientation yet, so stuff doesn't work. Used didRotateFromInterfaceOrientation instead. That fires at the right time.
- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
{
    //UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    //[dot removeFromSuperview];
    //[self drawImage];
}

-(CGRect)currentScreenBoundsDependOnOrientation
{
    NSString *reqSysVer = @"8.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        return [UIScreen mainScreen].bounds;
    
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds)  ;
    CGFloat height = CGRectGetHeight(screenBounds) ;
    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if(UIInterfaceOrientationIsPortrait(interfaceOrientation)){
        screenBounds.size = CGSizeMake(width, height);
        NSLog(@"Portrait Height: %f", screenBounds.size.height);
    }else if(UIInterfaceOrientationIsLandscape(interfaceOrientation)){
        screenBounds.size = CGSizeMake(height, width);
        NSLog(@"Landscape Height: %f", screenBounds.size.height);
    }
    
    return screenBounds ;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
